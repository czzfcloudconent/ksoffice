function errorR(e) {
	$('#errorModalButton').trigger("click");
	$("#errorModalTitle").html(e);
}


/////////excel///////////////
var el = document.getElementById('sortExcelFile');
el.addEventListener('change', handle, false);

function handle(e) {
	$("div[id='excelSort'] input[id='sortExcelFileId']").attr("value", new Date().getTime());
}
//预处理excelFields
$("#firstAction").click(function(event) {
	if ($("#sortExcelFile").prop("files")[0] == null || $("div[id='excelSort'] input[id='startNumber']")
		.val() == 0) {
		errorR("请填写完整的数据");
		return false;
	}
	let formData = new FormData();
	formData.append("file", $("#sortExcelFile").prop("files")[0]);
	formData.append("startNumber", $("div[id='excelSort'] input[id='startNumber']").val());
	formData.append("id", $("div[id='excelSort'] input[id='sortExcelFileId']").val());
	commonAjax("/excel/readField", "post", formData, null,
		function(res) {
			//调用前
			$("#firstAction").html("<i class='fas fa-circle-notch fa-spin'></i>")
		},
		function(res) {
			//成功
			// console.log(res);
			if (res.code == 200) {
				$("div[id='excelSort'] input[id='excelFields']").tagsinput('removeAll');
				let fields = "";
				for (var i = 0; i < res.fields.length; i++) {
					fields += "<option>" + res.fields[i] + "</option>";
					$("div[id='excelSort'] input[id='excelFields']").tagsinput('add', res.fields[i]);
				}
				$("#selectFields").html(fields);
				$("#selectFields").selectpicker('refresh');
				$("#selectFields").selectpicker('render');
			}
			if (res.code == 500) {
				errorR("请填写正确的数据");
			}
		},
		function(res) {
			errorR("请求失败！联系管理员。");
			//失败
		},
		function(res) {
			//结束
			$("#firstAction").html(
				"<span class='btn-inner--visible'>预处理</span>"
			);
		});
})
//excel排序
$("#sortExport").click(function() {
	let excelSortExportParams = new FormData();
	excelSortExportParams.append("id", $("div[id='excelSort'] input[id='sortExcelFileId']").val());
	excelSortExportParams.append("selectFields", $('#selectFields').selectpicker('val'));
	excelSortExportParams.append("field", $("div[id='excelSort'] input[id='excelFields']").tagsinput('items'));
	excelSortExportParams.append("sort", $("div[id='excelSort'] input[type='radio']:checked").val());
	excelSortExportParams.append("fieldFontSize", $("div[id='excelSort'] #excelSort-field-fontSize").text());
	excelSortExportParams.append("dataFontSize", $("div[id='excelSort'] #excelSort-data-fontSize").text());
	excelSortExportParams.append("fieldRowSize", $("div[id='excelSort'] #excelSort-field-rowSize").text());
	excelSortExportParams.append("dataRowSize", $("div[id='excelSort'] #excelSort-data-rowSize").text());
	commonExcelExport(excelSortExportParams, "/excel/sortExcel");
})
//电信审核
$("#dxShenHeSumbit").click(function() {
	let formData = new FormData();
	formData.append("oldFile", $("#dxShenHeUploadOld").prop("files")[0]);
	formData.append("newFile", $("#dxShenHeUploadNew").prop("files")[0]);
	commonAjax("/dxExcelInput", "post", formData, null,
		function(res) {
			//调用前
			$("#dxShenHeSumbit").attr("disabled", "true");
			$("#dxShenHeSumbit").html("<i class='fas fa-circle-notch fa-spin'></i>")
		},
		function(res) {
			//成功
			// console.log(res);
			if (res.code == 200) {
				$("#dxShenHeResult").val(JSON.stringify(res));
				console.log(res);
			}
			if (res.code == 500) {
				errorR("请填写正确的数据");
			}
		},
		function(res) {
			errorR("请求失败！联系管理员。");
			//失败
		},
		function(res) {
			//结束
			$("#dxShenHeSumbit").html(
				"<span class='btn-inner--visible'>审核</span>"
			);
			$("#dxShenHeSumbit").removeAttr("disabled");
		});
})
$("#dxOutPut").click(function() {
	let formData = new FormData();
	formData.append("oldFile", $("#dxShenHeUploadOld").prop("files")[0]);
	formData.append("newFile", $("#dxShenHeUploadNew").prop("files")[0]);
	commonExcelExport(formData, "/dxExcelOutput");
})

//移动审核
$("#yidShenHeSumbit").click(function() {
	let formData = new FormData();
	formData.append("oldFile", $("#yidShenHeUploadOld").prop("files")[0]);
	formData.append("newFile", $("#yidShenHeUploadNew").prop("files")[0]);
	commonAjax("/yidEntryInput", "post", formData, null,
		function(res) {
			//调用前
			$("#yidShenHeSumbit").attr("disabled", "true");
			$("#yidShenHeSumbit").html("<i class='fas fa-circle-notch fa-spin'></i>")
		},
		function(res) {
			//成功
			// console.log(res);
			if (res.code == 200) {
				$("#yidShenHeResult").val(JSON.stringify(res));
				console.log(res);
			}
			if (res.code == 500) {
				errorR("请填写正确的数据");
			}
		},
		function(res) {
			errorR("请求失败！联系管理员。");
			//失败
		},
		function(res) {
			//结束
			$("#yidShenHeSumbit").html(
				"<span class='btn-inner--visible'>审核</span>"
			);
			$("#yidShenHeSumbit").removeAttr("disabled");
		});
})
$("#yidOutPut").click(function() {
	let formData = new FormData();
	formData.append("oldFile", $("#yidShenHeUploadOld").prop("files")[0]);
	formData.append("newFile", $("#yidShenHeUploadNew").prop("files")[0]);
	commonExcelExport(formData, "/yidEntryOutput");
})

//部门考勤统计
$("#monthDataExPeople").click(function(){
	if($("#replaceCardExcel").prop("files")[0]==null||$("#monthDataExcel").prop("files")[0]==null){
		errorR("请上传考勤补卡或考勤月报表");
		return;
	}
	let formData = new FormData();
	formData.append("replaceCardExcel", $("#replaceCardExcel").prop("files")[0]);
	formData.append("monthDataExcel", $("#monthDataExcel").prop("files")[0]);
	commonAjax("/compute/monthDataExPeople", "post", formData, null,
		function(res) {
			//调用前
			$("#monthDataExPeople").attr("disabled", "true");
			$("#monthDataExPeople").html("<i class='fas fa-circle-notch fa-spin'></i>")
		},
		function(res) {
			//成功
			if (res.code == 200) {
				let resMap=res['data'];
				let resTextContent="";
			//	console.log(resMap);
			//	console.log(Object.getOwnPropertyNames(resMap).length)
				for(var k in resMap){
			//		console.log(k);
					resTextContent += k+"："+resMap[k]+"\n";
				}
				$("#monthDataExPeopleRes").val(resTextContent);
			}
			if (res.code == 500) {
				errorR("程序内部错误！联系管理员。");
			}
		},
		function(res) {
			errorR("请求失败！联系管理员。");
			//失败
		},
		function(res) {
			//结束
			$("#monthDataExPeople").html(
				"<span class='btn-inner--visible'>考勤异常人员</span>"
			);
			$("#monthDataExPeople").removeAttr("disabled");
		});
})

$("#departDataSum").click(function() {
	if($("#replaceCardExcel").prop("files")[0]==null||$("#monthDataExcel").prop("files")[0]==null){
		errorR("请上传考勤补卡或考勤月报表");
		return;
	}
	let formData = new FormData();
	formData.append("replaceCardExcel", $("#replaceCardExcel").prop("files")[0]);
	formData.append("monthDataExcel", $("#monthDataExcel").prop("files")[0]);
	commonExcelExport(formData, "/compute/departDataSum");
})

//员工排班
$("#scheduling").click(function() {
	let formData = new FormData();
	formData.append("lineManager", $("#schedulingLineManager").val());
	formData.append("depart", $("#schedulingDepart").selectpicker('val'));
	formData.append("yearMonth", $("#schedulingYearMonth").val());
	formData.append("scScheduling", $("#scScheduling").val());
	formData.append("ptScheduling", $("#ptScheduling").val());
	commonExcelExport(formData, "/compute/scheduling");
})


function importExcel(obj) {
	if (obj.files.length == 0) {
		return;
	}
	var wb; //读取完成的数据
	var rABS = false; //是否将文件读取为二进制字符串
	const IMPORTFILE_MAXSIZE = 10 * 1024; //这里可以自定义控制导入文件大小
	var suffix = obj.files[0].name.split(".")[1]
	$('#_file_path').val(obj.files[0].name);
	if (obj.files[0].size / 1024 > IMPORTFILE_MAXSIZE) {
		return;
	}
	var f = obj.files[0];
	var reader = new FileReader();
	reader.onload = function(e) {
		data = e.target.result;
		if (rABS) {
			wb = XLSX.read(btoa(fixdata(data)), { //手动转化
				type: 'base64'
			});
		} else {
			wb = XLSX.read(data, {
				type: 'binary'
			});
		}
		//wb.SheetNames[0]是获取Sheets中第一个Sheet的名字
		//wb.Sheets[Sheet名]获取第一个Sheet的数据
		var a = wb.SheetNames[0];
		var b = wb.Sheets[a]; //内容为方式2
		data = XLSX.utils.sheet_to_json(b); //内容为方式1
		if (!data || data == "") {
			console.log("数据为空");
			return;
		} else {
			console.log(data);
			//在这里执行对解析后数据的处理
			for (var i = 0; i < data.length; i++) {
					console.log(data[i]["姓名"]);
					console.log(data[i]["工号"]);
					if(data[i]["__EMPTY"]!=null){
						console.log(data[i]["__EMPTY"]);
						let sd=true,count=1;
						while(sd){
							if(data[i]["__EMPTY_"+count]!=null){
								console.log(data[i]["__EMPTY_"+count]);
								count++;
							}else{
								sd=false;
							}
						}
					}else{
						return;
					}
			}	
		}
	};
	if (rABS) {
		reader.readAsArrayBuffer(f);
	} else {
		reader.readAsBinaryString(f);
	}
	// console.log(fixdata(data));
}

function scSchedulingImportRead(file) {
	importExcel(file);
}

//抽奖
var facePaths = [];
var facePathsLength = 0;
var trophyPaths = [];
var trophyPathsLength = 0;
var drawCarouselPhotoStr = "";

function lDFaceUploadFiles() {
	$("#lDFaceUploadBtn").attr("disabled", "true");
	$("#lDFaceUploadBtn").html("<i class='fas fa-circle-notch fa-spin'></i>");
	/// get select files.
	let selectFiles = document.getElementById("lDFaceUpload").files;
	let count = 0;
	for (var file of selectFiles) {
		facePaths[count] = file.webkitRelativePath;
		count++;
	}
	selectFiles = null;
	$("#lDFaceUploadBtn").attr("class", "btn btn-block btn-secondary");
	$("#lDFaceUploadBtn").html("<i class='fa fa-upload'></i>已上传");
	facePathsLength = count + 1;
	console.log(facePaths);
}

function LDTrophyUploadFiles() {
	$("#LDTrophyUploadBtn").attr("disabled", "true");
	$("#LDTrophyUploadBtn").html("<i class='fas fa-circle-notch fa-spin'></i>");
	/// get select files.
	let selectFiles = document.getElementById("LDTrophyUpload").files;
	let count = 0;
	for (var file of selectFiles) {
		trophyPaths[count] = file.webkitRelativePath;
		if (count == 0) {
			drawCarouselPhotoStr += "<div class='carousel-item active'><div class='card mb-3'><img src='../draw/" +
				trophyPaths[count] + "' style='height: 430px;'></div></div>";
		} else {
			drawCarouselPhotoStr += "<div class='carousel-item'><div class='card mb-3'><img src='../draw/" +
				trophyPaths[count] + "' style='height: 430px;'></div></div>";
		}
		count++;
	}
	$("#drawCarouselPhoto").html(drawCarouselPhotoStr);
	$("#LDTrophyUploadBtn").attr("class", "btn btn-block btn-secondary");
	$("#LDTrophyUploadBtn").html("<i class='fa fa-upload'></i>已上传");
	trophyPathsLength = selectFiles.length;
}

$("#resetDrawParm").click(function() {
	facePaths = [];
	facePathsLength = 0;
	trophyPaths = [];
	trophyPathsLength = 0;
	$("#lDFaceUploadBtn").attr("class", "btn btn-block btn-outline-secondary");
	$("#LDTrophyUploadBtn").attr("class", "btn btn-block btn-outline-secondary");
	$("#lDFaceUploadBtn").html("<i class='fa fa-upload'></i>人物图片");
	$("#LDTrophyUploadBtn").html("<i class='fa fa-upload'></i>奖品图片");
	$("#lDFaceUploadBtn").removeAttr("disabled");
	$("#LDTrophyUploadBtn").removeAttr("disabled");
})

//进入抽奖页面
function enterDrawHtml() {

	if (facePathsLength == 0 || trophyPathsLength == 0) {
		errorR("请选择对应的图片文件夹！");
		return;
	}
	$("#drawHtml").css("background-image", "url('../draw/背景图.png')");
	$("#drawHtml").css("background-size", "100% 100%");
	$("#drawHtml").css("background-repeat", "no-repeat");
	$("#drawHtml").css("position", "absolute");
	$("#drawHtml").css("padding", "0px");
	$("#drawHtml").css("width", "100%");
	$("#drawHtml").css("height", "100%");
	$("#drawHtml").css("top", "0");
	$("#drawHtml").css("left", "0");
	$("#drawHtml").css("background-color", "black");

	let drawHw = $("#drawHtml").width();
	let drawHh = $("#drawHtml").height();
	let drawTFDw = $("#drawTFDiv").width();
	let drawTFDh = $("#drawTFDiv").height();

	console.log(drawHw);
	console.log(drawHh);
	console.log(drawTFDw);
	console.log(drawTFDh);
	if (drawHh > drawTFDh) {
		let mtop = (drawHh - drawTFDh) / 2;
		$("#drawTFDiv").css("margin-top", mtop + "px");
	}
	if (drawHw > drawTFDw) {
		let mleft = (drawHw - drawTFDw) / 2;
		$("#drawTFDiv").css("margin-left", mleft + "px");
	}

	$("#drawHtml").show();
	$("#drawCbtn").hide();
	$(".drawMsg").hide();
}

var audio = document.getElementById('drawBgmusic');
var startDraw = false;
var changedIndex = -1;
var changedName = null;
var changedPicPath = null;

function sleep(time) {
	return new Promise((resolve) => setTimeout(resolve, time));
}

function start() {
	if (facePathsLength == 0 || trophyPathsLength == 0) {
		errorR("请选择对应的图片文件夹！");
		return;
	}

	audio.play();
	startDraw = true;
	$("#drawSbtn").hide();
	$("#drawCbtn").show();
	changeImg();
}

function close() {
	audio.pause();
	startDraw = false;
	if (changedIndex != -1) {
		$("#drawFaceInfoMsg").html(changedName);
		$("#drawFaceInfoPic").attr("src", changedPicPath).show();
		facePaths.splice(changedIndex, 1);
		$("#drawFaceInfoBtn").click();
	}
	//console.log(facePaths.length);
	$("#drawCbtn").hide();
	$("#drawSbtn").show();
}

async function changeImg() {
	while (startDraw) {
		for (var i = 0; i < facePaths.length; i++) {
			if (!startDraw) {
				return;
			}
			changedPicPath = "../draw/" + facePaths[i];
			$("#drawFaceImg").attr("src", changedPicPath);
			changedIndex = i;
			changedName = facePaths[i].substring(3, facePaths[i].length - 4);
			await sleep(50);
		}
	}
}

$("#drawSbtn").click(function(e) {
	start()
})
$("#drawCbtn").click(function(e) {
	close()
})

$("#luckyDrawBody").on('keydown', function(event) {
	if (event.keyCode === 13 && startDraw == false) {
		start();
		event.preventDefault();
	} else if (event.keyCode === 13 && startDraw == true) {
		close();
		event.preventDefault();
	}
});